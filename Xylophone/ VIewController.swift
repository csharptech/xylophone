//
//  ViewController.swift
//  Xylophone
//
//  Created by Cliff Sharp on 2/27/2019.
//  Copyright © 2019 Cliff SHarp. All rights reserved.
//

import UIKit
import AVFoundation

enum SenderTag: String {
    case note1
    case note2
    case note3
    case note4
    case note5
    case note6
    case note7
}

class ViewController: UIViewController, AVAudioPlayerDelegate {
    
    var audioPlayer: AVAudioPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func notePressed(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            playSound(note: SenderTag.note1.rawValue)
        case 2:
            playSound(note: SenderTag.note2.rawValue)
        case 3:
            playSound(note: SenderTag.note3.rawValue)
        case 4:
            playSound(note: SenderTag.note4.rawValue)
        case 5:
            playSound(note: SenderTag.note5.rawValue)
        case 6:
            playSound(note: SenderTag.note6.rawValue)
        case 7:
            playSound(note: SenderTag.note7.rawValue)
        default:
            playSound(note: "")
        }
    }
    
    func playSound(note: String) {
        if let audioPlayer = audioPlayer, audioPlayer.isPlaying { audioPlayer.stop() }
        
        guard let soundURL = Bundle.main.url(forResource: note, withExtension: "wav") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
            audioPlayer?.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
}

